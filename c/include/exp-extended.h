#pragma once

// exp-extended -- floating point with extended exponent range
// Copyright (C) 2023 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifdef __cplusplus
#include "exp-extended.hpp"
#else

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>

/* ------------------------------------------------------------------ */
/* extra complex functions */

/* complex ldexp */

static inline
float _Complex
cldexpf(float _Complex z, int e)
{
  return CMPLXF(ldexpf(crealf(z), e), ldexpf(cimagf(z), e));
}

static inline
double _Complex
cldexp(double _Complex z, int e)
{
  return CMPLX(ldexp(creal(z), e), ldexp(cimag(z), e));
}

static inline
long double _Complex
cldexpl(long double _Complex z, int e)
{
  return CMPLXL(ldexpl(creall(z), e), ldexpl(cimagl(z), e));
}

/* complex magnitude squared */

static inline
float
cnormf(float _Complex z)
{
  float x = crealf(z);
  float y = cimagf(z);
  return x * x + y * y;
}

static inline
double
cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

static inline
long double
cnorml(long double _Complex z)
{
  long double x = creall(z);
  long double y = cimagl(z);
  return x * x + y * y;
}

/* complex oo-norm */

static inline
float
cabsmaxf(float _Complex z)
{
  float x = crealf(z);
  float y = cimagf(z);
  return fmaxf(fabsf(x), fabsf(y));
}

static inline
double
cabsmax(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return fmax(fabs(x), fabs(y));
}

static inline
long double
cabsmaxl(long double _Complex z)
{
  long double x = creall(z);
  long double y = cimagl(z);
  return fmaxl(fabsl(x), fabsl(y));
}

/* ------------------------------------------------------------------ */
/* high dynamic range real */

/* High dynamic range real data structure. */

struct ereal_s
{
  double m;
  int64_t e;
};
typedef struct ereal_s ereal_t;

/* Create a normalized ereal value.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
ereal(double m, int64_t e)
{
  int e_m;
  m = frexp(m, &e_m);
  return (ereal_t){ m, e_m + e };
}

/* Absolute value. */

static inline
ereal_t
e_abs(ereal_t x)
{
  return (ereal_t){ fabs(x.m), x.e };
}

/* Negation. */

static inline
ereal_t
e_neg(ereal_t x)
{
  return (ereal_t){ -x.m, x.e };
}

/* Addition.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
e_add(ereal_t x, ereal_t y)
{
  if (x.m == 0)
  {
    return y;
  }
  else if (y.m == 0)
  {
    return x;
  }
  else if (x.e >= y.e)
  {
    return ereal(x.m + ldexp(y.m, y.e - x.e), x.e);
  }
  else
  {
    return ereal(ldexp(x.m, x.e - y.e) + y.m, y.e);
  }
}

/* Subtraction. */

static inline
ereal_t
e_sub(ereal_t x, ereal_t y)
{
  return e_add(x, e_neg(y));
}

/* Multiplication by a power of two.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
e_mul_2exp(ereal_t x, int64_t e)
{
  return (ereal_t){ x.m, x.e + e };
}

/* Multiplication.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
e_mul(ereal_t x, ereal_t y)
{
  return ereal(x.m * y.m, x.e + y.e);
}

/* Multiplication by a double. */

static inline
ereal_t
e_mul_d(ereal_t x, double y)
{
  return ereal(x.m * y, x.e);
}

/* Squaring. */

static inline
ereal_t
e_sqr(ereal_t x)
{
  return e_mul(x, x);
}

/* Division.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
e_div(ereal_t x, ereal_t y)
{
  return ereal(x.m / y.m, x.e - y.e);
}

static inline
bool
e_fits_d(ereal_t x)
{
  double y = ldexp(x.m, x.e);
  ereal_t z = ereal(y, 0);
  return z.m == x.m && (x.m == 0 || z.e == x.e);
}

/* Comparison. */

static inline
int
d_cmp(double x, double y)
{
  return (x > y) - (x < y);
}

static inline
int
e_cmp(ereal_t x, ereal_t y)
{
  if (x.m == 0 || y.m == 0)
  {
    return d_cmp(x.m, y.m);
  }
  int e = x.e > y.e ? x.e : y.e;
  return d_cmp(ldexp(x.m, x.e - e), ldexp(y.m, y.e - e)); // FIXME check exponent overflow in subtraction
}

/* Maximum. */

static inline
ereal_t
e_max(ereal_t x, ereal_t y)
{
  if (e_cmp(x, y) >= 0)
  {
    return x;
  }
  else
  {
    return y;
  }
}

/* Minimum. */

static inline
ereal_t
e_min(ereal_t x, ereal_t y)
{
  if (e_cmp(x, y) <= 0)
  {
    return x;
  }
  else
  {
    return y;
  }
}

/* Square root.
FIXME Should check for overflow in exponent operations. */

static inline
ereal_t
e_sqrt(ereal_t x)
{
  if (x.e & 1)
  {
    return ereal(sqrt(2 * x.m), (x.e - 1) / 2);
  }
  else
  {
    return ereal(sqrt(x.m), x.e / 2);
  }
}

static inline
ereal_t
e_0(void)
{
  return (ereal_t){ 0, 0 };
}

static inline
ereal_t
e_1(void)
{
  return (ereal_t){ 0.5, 1 };
}

static inline
ereal_t
e_inf(void)
{
  return (ereal_t){ 1.0/0.0, 0 };
}

static inline
ereal_t
e_ninf(void)
{
  return (ereal_t){ -1.0/0.0, 0 };
}

static inline
bool
e_isinf(ereal_t x)
{
  return isinf(x.m);
}

static inline
bool
e_isnan(ereal_t x)
{
  return isnan(x.m);
}

static inline
int
e_fpclassify(ereal_t x)
{
  return fpclassify(x.m);
}

static inline
ereal_t
e_from_d(double m)
{
  return ereal(m, 0);
}

static inline
double
d_from_e(ereal_t x)
{
  return ldexp(x.m, x.e);
}

static inline
int
e_sgn(ereal_t x)
{
  return d_cmp(x.m, 0);
}

static inline
ereal_t
e_pow_2exp(ereal_t x, int64_t e)
{
  if (e >= 0)
  {
    for (int64_t i = 0; i < e; ++i)
    {
      x = e_sqr(x);
    }
  }
  else
  {
    for (int64_t i = 0; i < -e; ++i)
    {
      x = e_sqrt(x);
    }
  }
  return x;
}

static inline
ereal_t
e_exp(ereal_t x)
{
  if (x.m == 0)
  {
    return e_1();
  }
  else if (isnan(x.m))
  {
    return x;
  }
  else if (30 < x.e || isinf(x.m))
  {
    if (x.m > 0)
    {
      return e_inf();
    }
    else
    {
      return e_0();
    }
  }
  else if (-50 < x.e && x.e < 10)
  {
    return e_from_d(exp(d_from_e(x)));
  }
  else if (x.e <= -50)
  {
    return e_1();
  }
  else
  {
    return e_pow_2exp(e_from_d(exp(x.m)), x.e);
  }
}

/* ------------------------------------------------------------------ */
/* high dynamic range complex */

/* High dynamic range complex data structure. */

struct ecomplex_s
{
  double _Complex m;
  int64_t e;
};
typedef struct ecomplex_s ecomplex_t;

/* Create a normalized ecomplex value.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ecomplex(double _Complex m, int64_t e)
{
  int ex, ey;
  double x = frexp(creal(m), &ex);
  double y = frexp(cimag(m), &ey);
  if (x == 0 && y == 0)
  {
    return (ecomplex_t){ 0, 0 };
  }
  else if (x == 0)
  {
    return (ecomplex_t){ I * y, e + ey };
  }
  else if (y == 0)
  {
    return (ecomplex_t){ x, e + ex };
  }
  else if (ex >= ey)
  {
    return (ecomplex_t){ x + I * ldexp(y, ey - ex), e + ex };
  }
  else
  {
    return (ecomplex_t){ ldexp(x, ex - ey) + I * y, e + ey };
  }
}

static inline
bool
ec_fits_dc(ecomplex_t x)
{
  double _Complex y = cldexp(x.m, x.e);
  ecomplex_t z = ecomplex(y, 0);
  return z.m == x.m && (x.m == 0 || z.e == x.e);
}

/* Multiplication.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ec_mul(ecomplex_t x, ecomplex_t y)
{
  return ecomplex(x.m * y.m, x.e + y.e);
}

/* Division.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ec_div(ecomplex_t x, ecomplex_t y)
{
  return ecomplex(x.m / y.m, x.e - y.e);
}

/* Squaring. */

static inline
ecomplex_t
ec_sqr(ecomplex_t x)
{
  return ec_mul(x, x);
}

/* Multiplication by a power of two.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ec_mul_2exp(ecomplex_t x, int64_t e)
{
  return (ecomplex_t){ x.m, x.e + e };
}

/* Negation.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ec_neg(ecomplex_t x)
{
  return (ecomplex_t){ -x.m, x.e };
}

/* Addition.
FIXME Should check for overflow in exponent operations. */

static inline
ecomplex_t
ec_add(ecomplex_t x, ecomplex_t y)
{
  if (x.m == 0)
  {
    return y;
  }
  else if (y.m == 0)
  {
    return x;
  }
  else if (x.e >= y.e)
  {
    return ecomplex(x.m + cldexp(y.m, y.e - x.e), x.e);
  }
  else
  {
    return ecomplex(cldexp(x.m, x.e - y.e) + y.m, y.e);
  }
}

/* Subtraction. */

static inline
ecomplex_t
ec_sub(ecomplex_t x, ecomplex_t y)
{
  return ec_add(x, ec_neg(y));
}

/* Magnitude squared.
FIXME Check exponent for overflow. */

static inline
ereal_t
ec_norm(ecomplex_t x)
{
  return ereal(cnorm(x.m), 2 * x.e);
}

/* oo-norm. */

static inline
ereal_t
ec_absmax(ecomplex_t x)
{
  return e_max(e_abs(ereal(creal(x.m), x.e)), e_abs(ereal(cimag(x.m), x.e)));
}

#endif
